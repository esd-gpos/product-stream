package product_stream

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type MongoUpdateRepo struct {
	Col *mongo.Collection
}

func (m *MongoUpdateRepo) Update(ctx context.Context, prodEntity *ProductEntity) error {
	_, err := m.Col.UpdateMany(ctx, bson.M{"product_id": prodEntity.ProductId}, bson.M{
		"$set": bson.M{
			"name":  prodEntity.Name,
			"tags":  prodEntity.Tags,
			"price": prodEntity.Price,
			"desc":  prodEntity.Desc,
		},
	})
	if err != nil {
		return err
	}

	return nil
}
