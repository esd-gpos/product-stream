package product_stream

import (
	"context"
	"io/ioutil"
)

type FileTokenRepo struct {
	Filename string
}

func (f *FileTokenRepo) Write(ctx context.Context, token []byte) error {
	if err := ioutil.WriteFile(f.Filename, token, 0666); err != nil {
		return err
	}

	return nil
}

func (f *FileTokenRepo) Token(ctx context.Context) ([]byte, error) {
	token, err := ioutil.ReadFile(f.Filename)
	if err != nil {
		return nil, err
	}

	return token, nil
}
