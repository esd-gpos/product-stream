package product_stream

import (
	"bytes"
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

const opReplace = "replace"

// Represent a product
type ProductEntity struct {
	ProductId string   `bson:"_id"`
	Name      string   `bson:"name"`
	Tags      []string `bson:"tags"`
	Price     float32  `bson:"price"`
	Desc      string   `bson:"desc"`
}

// Represent data from the Mongo Change Stream
type MongoStreamEntity struct {
	FullDocument  ProductEntity `bson:"fullDocument"`
	OperationType string        `bson:"operationType"`
}

// Is used to update product to other database and collections
type UpdateRepo interface {
	Update(ctx context.Context, prodEntity *ProductEntity) error
}

// Persist the resume token to some storage
type TokenRepo interface {
	Write(ctx context.Context, token []byte) error
	Token(ctx context.Context) ([]byte, error)
}

// Watches a Mongodb Change Stream
func WatchMongoStream(ctx context.Context, col *mongo.Collection, update UpdateRepo, token TokenRepo) error {
	// Retrieve the latest resume token first
	log.Print("Retrieving resume token")

	tk, err := token.Token(ctx)
	if err != nil {
		return err
	}

	// Create ChangeStream
	log.Println("Creating Change Stream")

	ops := make([]*options.ChangeStreamOptions, 0)
	if len(tk) != 0 {
		temp, err := bson.NewFromIOReader(bytes.NewReader(tk))
		if err != nil {
			return err
		}

		ops = append(ops, options.ChangeStream().SetResumeAfter(temp))
	}

	cs, err := col.Watch(ctx, mongo.Pipeline{}, ops...)
	if err != nil {
		return err
	}

	// Start reading stream
	log.Println("Start watching stream")

	for cs.Next(ctx) {
		// Decode into entity
		streamEntity := &MongoStreamEntity{}

		err := cs.Decode(streamEntity)
		if err != nil {
			return err
		}

		// Propagate update
		if streamEntity.OperationType == opReplace {
			if err = update.Update(ctx, &streamEntity.FullDocument); err != nil {
				return err
			}
		}

		// Update token
		if err = token.Write(ctx, cs.ResumeToken()); err != nil {
			return err
		}

		log.Printf("Product %s updated\n", streamEntity.FullDocument.ProductId)
	}

	return nil
}
