package main

import (
	"context"
	"github.com/spf13/viper"
	"gitlab.com/esd-gpos/product-stream/product_stream"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"strings"
	"time"
)

const (
	filename   = "token"
	configFile = "main.env"
	configPath = "."
)

func main() {
	// Load config
	loadConfig(configFile, configPath)

	// Get env variables
	mongoProdUri := viper.GetString("MONGO_PROD_URI")
	mongoProdDb := viper.GetString("MONGO_PROD_DB")
	mongoProdCol := viper.GetString("MONGO_PROD_COLLECTION")

	mongoItemUri := viper.GetString("MONGO_ITEM_URI")
	mongoItemDb := viper.GetString("MONGO_ITEM_DB")
	mongoItemCol := viper.GetString("MONGO_ITEM_COLLECTION")

	failOnEmpty(mongoProdUri, mongoProdDb, mongoProdCol, mongoItemUri, mongoItemDb, mongoItemCol)

	// Connect to Mongo Prod collection
	mongoProdClient, err := mongo.NewClient(options.Client().ApplyURI(mongoProdUri))
	failOnError("could not create mongo client", err)

	timeout, cancel := context.WithTimeout(context.Background(), time.Second*20)
	defer cancel()

	err = mongoProdClient.Connect(timeout)
	failOnError("mongo client could not connect", err)

	mongoProdCollection := mongoProdClient.Database(mongoProdDb).Collection(mongoProdCol)

	// Connect to Mongo Item collection
	mongoItemClient, err := mongo.NewClient(options.Client().ApplyURI(mongoItemUri))
	failOnError("could not create mongo client", err)

	timeout, cancel = context.WithTimeout(context.Background(), time.Second*20)
	defer cancel()

	err = mongoItemClient.Connect(timeout)
	failOnError("mongo client could not connect", err)

	mongoItemCollection := mongoItemClient.Database(mongoItemDb).Collection(mongoItemCol)

	// Create Repo
	updateItem := &product_stream.MongoUpdateRepo{Col: mongoItemCollection}
	fileToken := &product_stream.FileTokenRepo{Filename: filename}

	log.Println("Begin")
	log.Fatal(product_stream.WatchMongoStream(context.Background(), mongoProdCollection, updateItem, fileToken))
}

func loadConfig(filename string, path string) {
	viper.SetConfigFile(filename)
	viper.AddConfigPath(path)

	if err := viper.ReadInConfig(); err != nil {
		log.Fatal("could not read config")
	}
}

func failOnEmpty(value ...string) {
	for _, v := range value {
		if strings.TrimSpace(v) == "" {
			log.Fatal("some value is missing")
		}
	}
}

func failOnError(msg string, err error) {
	if err != nil {
		log.Fatalf("%s:%s", msg, err)
	}
}
