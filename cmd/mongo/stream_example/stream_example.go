package main

import (
	"context"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"strings"
	"time"
)

type mapping map[string]interface{}

func main() {
	// Load config
	configFile := "example.env"
	configPath := "."

	loadConfig(configFile, configPath)

	// Get env variables
	mongoUri := viper.GetString("MONGO_URI")
	mongoDb := viper.GetString("MONGO_DB")
	mongoCol := viper.GetString("MONGO_COLLECTION")

	failOnEmpty(mongoUri, mongoDb, mongoCol)

	// Connect to Mongo collection
	mongoClient, err := mongo.NewClient(options.Client().ApplyURI(mongoUri))
	failOnError("could not create mongo client", err)

	timeout, cancel := context.WithTimeout(context.Background(), time.Second*20)
	defer cancel()

	err = mongoClient.Connect(timeout)
	failOnError("mongo client could not connect", err)

	mongoCollection := mongoClient.Database(mongoDb).Collection(mongoCol)

	// Read stream of data
	cs, err := mongoCollection.Watch(context.Background(), mongo.Pipeline{})
	failOnError("could not watch mongo collection", err)

	defer cs.Close(context.Background())

	for cs.Next(context.Background()) {
		body := mapping{}

		err := cs.Decode(&body)
		failOnError("could not decode", err)

		log.Printf("%s \n\n", body)
	}
}

func loadConfig(filename string, path string) {
	viper.SetConfigFile(filename)
	viper.AddConfigPath(path)

	if err := viper.ReadInConfig(); err != nil {
		log.Fatal("could not read config")
	}
}

func failOnEmpty(value ...string) {
	for _, v := range value {
		if strings.TrimSpace(v) == "" {
			log.Fatal("some value is missing")
		}
	}
}

func failOnError(msg string, err error) {
	if err != nil {
		log.Fatalf("%s:%s", msg, err)
	}
}
