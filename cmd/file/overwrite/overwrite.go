package main

import (
	"io/ioutil"
	"log"
	"os"
)

func main() {
	if err := ioutil.WriteFile("test.txt", []byte(os.Args[1]), 0666); err != nil {
		log.Fatal(err)
	}
}
