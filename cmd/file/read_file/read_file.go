package main

import (
	"io/ioutil"
	"log"
)

func main() {
	if _, err := ioutil.ReadFile("test.txt"); err != nil {
		log.Fatal(err)
	}
}
